package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id= UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients=new ArrayList<Ingredient>();
	
	public Pizza(){
		
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Pizza pizza = (Pizza) o;
		return Objects.equals(id, pizza.id) && Objects.equals(name, pizza.name) && Objects.equals(ingredients, pizza.ingredients);
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza res=new Pizza();
		res.setId(dto.getId());
		res.setName(dto.getName());
		List<Ingredient> list=new ArrayList<Ingredient>();
		if(dto.getIngredients().size()>0){
			for(IngredientDto ingr:dto.getIngredients()){
				list.add(Ingredient.fromDto(ingr));
			}
		}

		res.setIngredients(list);
		
		return res;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto res= new PizzaDto();
		res.setId(p.getId());
		res.setName(p.name);
		List<IngredientDto> list=new ArrayList<IngredientDto>();
		if(p.getIngredients().size()>0){
			for(Ingredient ingr:p.getIngredients()){
				list.add(Ingredient.toDto(ingr));
			}
		}

		res.setIngredients(list);
		
		return res;
	}

	public static PizzaCreateDto toCreateDto(Pizza p){
		PizzaCreateDto res=new PizzaCreateDto();
		res.setName(p.getName());
		List<IngredientDto> list =new ArrayList<IngredientDto>();
		if (p.getIngredients().size()!=0){
			for(Ingredient ingr:p.getIngredients()){
				list.add(Ingredient.toDto(ingr));
			}
		}

		res.setIngredients(list);

		return res;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto){
		Pizza res=new Pizza();
		res.setName(dto.getName());
		List<Ingredient> list= new ArrayList<Ingredient>();
		if (dto.getIngredients().size()>0){
			for(IngredientDto ingr:dto.getIngredients()){
				list.add(Ingredient.fromDto(ingr));
			}
		}

		res.setIngredients(list);

		return res;

	}
	
	@Override
	public String toString() {
		return "Pizza["
				+"id="+this.id
				+"name="+this.name
				+this.ingredients.toString()
				+ "]";
	}
}
