package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pizza_id VARCHAR(128) FOREIGN KEY REFERENCES Pizza(id), ingredient_id VARCHAR(128) FOREIGN KEY REFERENCES ingredients(id))")
    void createAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
      createAssociationTable();
      createPizzaTable();
    }
    
    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();

    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();

    @Transaction
    default void dropTableAndIngredientAssociation(){
        dropPizzaTable();
        dropAssociationTable();
    }
    
    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);


    @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

}
