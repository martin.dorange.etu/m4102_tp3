package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class PizzaCreateDto {
    private String name;
    private List<IngredientDto> ingredients=new ArrayList<IngredientDto>();

    public PizzaCreateDto() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIngredients(List<IngredientDto> l){
        this.ingredients=l;
    }

    public List<IngredientDto> getIngredients(){
        return this.ingredients;
    }
}
