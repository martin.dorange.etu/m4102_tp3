```
<invocation> ::= "CALL:" <methode> | <methode> ":" <param> [ ":" <param> ]*
<methode> ::= <chaine>
<param> ::= "param[" <type> "," <valeur> "]"
<type> ::= "char" | "string" | "int" | "float"
<valeur> ::= <char> | <chaine> | [0-9]* | [0-9]* "." [0-9]*
<chaine> ::= "<char>*"
<char> ::= un charactère alphanumérique
<reponse> ::= "RETURN:" <type> ":" <valeur>
```

```  
<>
```